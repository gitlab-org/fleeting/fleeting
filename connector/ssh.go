package connector

import (
	"context"
	"errors"
	"fmt"
	"io"
	"net"
	"strings"
	"sync"
	"sync/atomic"
	"time"

	"golang.org/x/crypto/ssh"

	"gitlab.com/gitlab-org/fleeting/fleeting/provider"
)

type sshClient struct {
	client *ssh.Client
	done   chan struct{}

	mu     sync.Mutex
	closed bool
}

const (
	// handshakeErrorRetryInterval is how often to retry a connection
	// after a handshake/initial temporary connection error.
	handshakeErrorRetryInterval = 2 * time.Second
)

func DialSSH(ctx context.Context, info provider.ConnectInfo, options DialOptions) (*sshClient, error) {
	addr := info.InternalAddr
	if options.UseExternalAddr && (info.ExternalAddr != "" || addr == "") {
		addr = info.ExternalAddr
	}
	addr = hostport(addr, "22")

	// By default, the Golang SSH defaults leave out diffie-hellman-group-exchange-sha{1,256}
	// from the list of preferred key exchanges for a number of reasons:
	// 1. To keep the list of preferred key exchanges to a small, well-curated list.
	//    See https://github.com/golang/go/issues/17230#issuecomment-485801815.
	// 2. Only the client implementation is supported.
	//
	// Add it here to provide maximum compatiblity for SSH servers.
	sshConf := ssh.Config{}
	sshConf.SetDefaults()
	sshConf.KeyExchanges = append(
		sshConf.KeyExchanges,
		"diffie-hellman-group-exchange-sha1",
		"diffie-hellman-group-exchange-sha256",
	)

	config := &ssh.ClientConfig{
		Config:          sshConf,
		User:            info.Username,
		HostKeyCallback: ssh.InsecureIgnoreHostKey(),
		Timeout:         info.Timeout,
	}

	if info.Key == nil {
		config.Auth = append(config.Auth, ssh.Password(info.Password))
	} else {
		signer, err := ssh.ParsePrivateKey(info.Key)
		if err != nil {
			return nil, err
		}
		config.Auth = append(config.Auth, ssh.PublicKeys(signer))
	}

	dialer := dialer{
		Timeout:   info.Timeout,
		KeepAlive: info.Keepalive,
		DialFn:    options.DialFn,
	}

	ioTimeout := info.Timeout

	var client *ssh.Client
	var err error
	i := 0
	for {
		startedDial := time.Now()
		client, err = getSSHClient(ctx, dialer, addr, config, ioTimeout)
		if err != nil {
			// check connect info expiration
			if info.Expires != nil && info.Expires.Before(time.Now()) {
				return nil, fmt.Errorf("%w: %w", err, ErrConnectInfoExpired)
			}
		}

		// the io timeout is shared between retries, so we remove how long
		// the last attempt took
		ioTimeout -= time.Since(startedDial)

		if ctx.Err() == nil && err != nil && isTemporarySSHError(err) && ioTimeout > 0 {
			time.Sleep(handshakeErrorRetryInterval)
			i++
			continue
		}
		break
	}
	if err := errors.Join(ctx.Err(), err); err != nil {
		return nil, fmt.Errorf("after retrying %d times during %v timeout: %w", i, info.Timeout, err)
	}

	sc := &sshClient{client: client, done: make(chan struct{})}
	go sc.keepalive(info.Keepalive)

	return sc, nil
}

func isTemporarySSHError(err error) bool {
	str := err.Error()

	return strings.Contains(str, "handshake failed") || strings.Contains(str, "i/o timeout")
}

func (c *sshClient) keepalive(interval time.Duration) {
	for {
		select {
		case <-c.done:
			return
		default:
			time.Sleep(interval)
			_, _, _ = c.client.SendRequest("keepalive@openssh.com", true, nil)
		}
	}
}

func (c *sshClient) Close() error {
	c.mu.Lock()
	defer c.mu.Unlock()

	if c.closed {
		return fmt.Errorf("connection already closed")
	}

	c.closed = true
	close(c.done)

	return c.client.Close()
}

func (c *sshClient) Run(ctx context.Context, opts RunOptions) error {
	sess, err := c.client.NewSession()
	if err != nil {
		return err
	}
	defer sess.Close()

	var stderrWriter *stderrOmitWriter
	if opts.Stderr == nil {
		stderrWriter = newStderrOmitWriter()
		opts.Stderr = stderrWriter
	}

	sess.Stdin = opts.Stdin
	sess.Stdout = opts.Stdout
	sess.Stderr = opts.Stderr

	errCh := make(chan error, 1)

	go func() {
		errCh <- sess.Run(opts.Command)
	}()

	select {
	case <-ctx.Done():
		return errors.Join(ctx.Err(), sess.Close())
	case err := <-errCh:
		if err == nil {
			return nil
		}

		if stderrWriter != nil {
			err = errors.Join(err, stderrWriter.Error())
		}

		return c.newExitError(err)
	}
}

func (c *sshClient) newExitError(err error) error {
	if err == nil {
		return nil
	}

	var sshExitErr *ssh.ExitError
	if errors.As(err, &sshExitErr) {
		return &ExitError{
			err:      err,
			exitCode: sshExitErr.ExitStatus(),
		}
	}

	return &ExitError{err: err, exitCode: 0}
}

func (c *sshClient) DialRun(ctx context.Context, command string) (net.Conn, error) {
	connReader, w := io.Pipe()
	r, connWriter := io.Pipe()

	go func() {
		ctx, cancel := context.WithCancel(ctx)
		defer cancel()

		defer r.Close()
		defer w.Close()

		stderr := newStderrOmitWriter()
		err := c.Run(ctx, RunOptions{
			Command: command,
			Stdin:   r,
			Stdout:  w,
			Stderr:  stderr,
		})
		if err != nil {
			err = fmt.Errorf("ssh tunnel: %w (%s)", err, stderr.Error())
			r.CloseWithError(err)
			w.CloseWithError(err)
		}
	}()

	return &rwConn{connWriter, connReader}, nil
}

func (c *sshClient) Dial(network string, address string) (net.Conn, error) {
	return c.client.Dial(network, address)
}

func getSSHClient(ctx context.Context, d dialer, address string, config *ssh.ClientConfig, ioTimeout time.Duration) (*ssh.Client, error) {
	ctx, cancel := context.WithTimeout(ctx, ioTimeout)
	defer cancel()

	conn, err := d.DialContext(ctx, "tcp", address)
	if err != nil {
		return nil, err
	}

	// wait until the context is canceled:
	// if success is true, we don't close the connection
	// if success if false, we close the connection as it stops NewClientConn
	// from continuing to block.
	var success atomic.Bool
	go func() {
		<-ctx.Done()
		if !success.Load() {
			conn.Close()
		}
	}()

	defer conn.SetDeadline(time.Time{})

	// setting dial deadline is best effort, if the context is canceled,
	// we close the entire connection anyway.
	_ = conn.SetDeadline(time.Now().Add(ioTimeout))

	c, chans, reqs, err := ssh.NewClientConn(conn, address, config)
	if err != nil {
		return nil, err
	}

	success.Store(true)

	return ssh.NewClient(c, chans, reqs), nil
}
