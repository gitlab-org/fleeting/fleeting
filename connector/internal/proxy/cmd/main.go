package main

import (
	"fmt"
	"os"
	"time"
)

func main() {
	if len(os.Args) < 3 {
		fmt.Fprintf(os.Stderr, "usage: %s <network> <address>\n", os.Args[0])
		return
	}

	var err error
	timeout := time.Minute
	if len(os.Args) == 4 {
		timeout, err = time.ParseDuration(os.Args[3])
		if err != nil {
			fmt.Fprintln(os.Stderr, err)
		}
	}

	err = proxy(os.Stdout, os.Stdin, os.Args[1], os.Args[2], timeout)
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
	}
}
