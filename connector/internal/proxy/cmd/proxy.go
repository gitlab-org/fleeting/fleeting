package main

import (
	"errors"
	"fmt"
	"io"
	"net"
)

func do(w io.Writer, r io.Reader, conn net.Conn, closedErr error) error {
	defer conn.Close()

	recvCh := make(chan error, 1)
	sendCh := make(chan error, 1)
	go func() {
		recvCh <- copier(conn, r, "stdin to conn")
	}()
	go func() {
		err := copier(w, conn, "conn to stdout")
		if errors.Is(err, closedErr) {
			err = nil
		}
		sendCh <- err
	}()

	var err error
	select {
	case err = <-recvCh:
		if err != nil {
			return err
		}
		// wait for stdout
		err = <-sendCh
	case err = <-sendCh:
		// closing on defer will stop stdin to conn go routine
	}

	return err
}

func copier(to io.Writer, from io.Reader, desc string) (err error) {
	defer func() {
		if t, ok := from.(interface{ CloseRead() error }); ok {
			if cerr := t.CloseRead(); cerr != nil && err == nil {
				err = fmt.Errorf("close reader (%s): %w", desc, cerr)
			}
		}

		if t, ok := to.(interface{ CloseWrite() error }); ok {
			if cerr := t.CloseWrite(); cerr != nil && err == nil {
				err = fmt.Errorf("close writer (%s): %w", desc, cerr)
			}
		}
	}()

	if _, err := io.Copy(to, from); err != nil {
		return fmt.Errorf("copy (%s): %w", desc, err)
	}

	return nil
}
