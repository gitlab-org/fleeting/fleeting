package integration

import (
	"testing"

	dummy "gitlab.com/gitlab-org/fleeting/fleeting/plugin/fleeting-plugin-dummy"
	"gitlab.com/gitlab-org/fleeting/fleeting/provider"
)

func TestIntegration(t *testing.T) {
	binaryName := BuildPluginBinary(t, "../plugin/fleeting-plugin-dummy/cmd/fleeting-plugin-dummy", "fleeting-plugin-dummy")

	TestProvisioning(t, binaryName, Config{
		PluginConfig: dummy.InstanceGroup{
			ListenSSH:        true,
			SimulateDeleting: true,
		},
		ConnectorConfig: provider.ConnectorConfig{
			Username:             "username",
			Password:             "password",
			UseStaticCredentials: true,
		},
		MaxInstances:    3,
		UseExternalAddr: false,
	})
}
