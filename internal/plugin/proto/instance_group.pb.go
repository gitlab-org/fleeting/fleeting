// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.34.1
// 	protoc        v4.22.2
// source: proto/instance_group.proto

package proto

import (
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type UpdateResponse_State int32

const (
	UpdateResponse_creating UpdateResponse_State = 0
	UpdateResponse_running  UpdateResponse_State = 1
	UpdateResponse_deleting UpdateResponse_State = 2
	UpdateResponse_deleted  UpdateResponse_State = 3
	UpdateResponse_timeout  UpdateResponse_State = 4
)

// Enum value maps for UpdateResponse_State.
var (
	UpdateResponse_State_name = map[int32]string{
		0: "creating",
		1: "running",
		2: "deleting",
		3: "deleted",
		4: "timeout",
	}
	UpdateResponse_State_value = map[string]int32{
		"creating": 0,
		"running":  1,
		"deleting": 2,
		"deleted":  3,
		"timeout":  4,
	}
)

func (x UpdateResponse_State) Enum() *UpdateResponse_State {
	p := new(UpdateResponse_State)
	*p = x
	return p
}

func (x UpdateResponse_State) String() string {
	return protoimpl.X.EnumStringOf(x.Descriptor(), protoreflect.EnumNumber(x))
}

func (UpdateResponse_State) Descriptor() protoreflect.EnumDescriptor {
	return file_proto_instance_group_proto_enumTypes[0].Descriptor()
}

func (UpdateResponse_State) Type() protoreflect.EnumType {
	return &file_proto_instance_group_proto_enumTypes[0]
}

func (x UpdateResponse_State) Number() protoreflect.EnumNumber {
	return protoreflect.EnumNumber(x)
}

// Deprecated: Use UpdateResponse_State.Descriptor instead.
func (UpdateResponse_State) EnumDescriptor() ([]byte, []int) {
	return file_proto_instance_group_proto_rawDescGZIP(), []int{5, 0}
}

type Empty struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields
}

func (x *Empty) Reset() {
	*x = Empty{}
	if protoimpl.UnsafeEnabled {
		mi := &file_proto_instance_group_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *Empty) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*Empty) ProtoMessage() {}

func (x *Empty) ProtoReflect() protoreflect.Message {
	mi := &file_proto_instance_group_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use Empty.ProtoReflect.Descriptor instead.
func (*Empty) Descriptor() ([]byte, []int) {
	return file_proto_instance_group_proto_rawDescGZIP(), []int{0}
}

type Settings struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	ConnectorConfig *ConnectorConfig `protobuf:"bytes,1,opt,name=connector_config,json=connectorConfig,proto3" json:"connector_config,omitempty"`
}

func (x *Settings) Reset() {
	*x = Settings{}
	if protoimpl.UnsafeEnabled {
		mi := &file_proto_instance_group_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *Settings) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*Settings) ProtoMessage() {}

func (x *Settings) ProtoReflect() protoreflect.Message {
	mi := &file_proto_instance_group_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use Settings.ProtoReflect.Descriptor instead.
func (*Settings) Descriptor() ([]byte, []int) {
	return file_proto_instance_group_proto_rawDescGZIP(), []int{1}
}

func (x *Settings) GetConnectorConfig() *ConnectorConfig {
	if x != nil {
		return x.ConnectorConfig
	}
	return nil
}

type ConnectorConfig struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Os                   string `protobuf:"bytes,1,opt,name=os,proto3" json:"os,omitempty"`
	Arch                 string `protobuf:"bytes,2,opt,name=arch,proto3" json:"arch,omitempty"`
	Protocol             string `protobuf:"bytes,3,opt,name=protocol,proto3" json:"protocol,omitempty"`
	Username             string `protobuf:"bytes,4,opt,name=username,proto3" json:"username,omitempty"`
	Password             string `protobuf:"bytes,5,opt,name=password,proto3" json:"password,omitempty"`
	Key                  []byte `protobuf:"bytes,6,opt,name=key,proto3" json:"key,omitempty"`
	UseStaticCredentials bool   `protobuf:"varint,7,opt,name=useStaticCredentials,proto3" json:"useStaticCredentials,omitempty"`
	Keepalive            int64  `protobuf:"varint,8,opt,name=keepalive,proto3" json:"keepalive,omitempty"`
	Timeout              int64  `protobuf:"varint,9,opt,name=timeout,proto3" json:"timeout,omitempty"`
}

func (x *ConnectorConfig) Reset() {
	*x = ConnectorConfig{}
	if protoimpl.UnsafeEnabled {
		mi := &file_proto_instance_group_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *ConnectorConfig) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*ConnectorConfig) ProtoMessage() {}

func (x *ConnectorConfig) ProtoReflect() protoreflect.Message {
	mi := &file_proto_instance_group_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use ConnectorConfig.ProtoReflect.Descriptor instead.
func (*ConnectorConfig) Descriptor() ([]byte, []int) {
	return file_proto_instance_group_proto_rawDescGZIP(), []int{2}
}

func (x *ConnectorConfig) GetOs() string {
	if x != nil {
		return x.Os
	}
	return ""
}

func (x *ConnectorConfig) GetArch() string {
	if x != nil {
		return x.Arch
	}
	return ""
}

func (x *ConnectorConfig) GetProtocol() string {
	if x != nil {
		return x.Protocol
	}
	return ""
}

func (x *ConnectorConfig) GetUsername() string {
	if x != nil {
		return x.Username
	}
	return ""
}

func (x *ConnectorConfig) GetPassword() string {
	if x != nil {
		return x.Password
	}
	return ""
}

func (x *ConnectorConfig) GetKey() []byte {
	if x != nil {
		return x.Key
	}
	return nil
}

func (x *ConnectorConfig) GetUseStaticCredentials() bool {
	if x != nil {
		return x.UseStaticCredentials
	}
	return false
}

func (x *ConnectorConfig) GetKeepalive() int64 {
	if x != nil {
		return x.Keepalive
	}
	return 0
}

func (x *ConnectorConfig) GetTimeout() int64 {
	if x != nil {
		return x.Timeout
	}
	return 0
}

type InitRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Config   []byte    `protobuf:"bytes,1,opt,name=config,proto3" json:"config,omitempty"`
	Settings *Settings `protobuf:"bytes,2,opt,name=settings,proto3" json:"settings,omitempty"`
}

func (x *InitRequest) Reset() {
	*x = InitRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_proto_instance_group_proto_msgTypes[3]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *InitRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*InitRequest) ProtoMessage() {}

func (x *InitRequest) ProtoReflect() protoreflect.Message {
	mi := &file_proto_instance_group_proto_msgTypes[3]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use InitRequest.ProtoReflect.Descriptor instead.
func (*InitRequest) Descriptor() ([]byte, []int) {
	return file_proto_instance_group_proto_rawDescGZIP(), []int{3}
}

func (x *InitRequest) GetConfig() []byte {
	if x != nil {
		return x.Config
	}
	return nil
}

func (x *InitRequest) GetSettings() *Settings {
	if x != nil {
		return x.Settings
	}
	return nil
}

type InitResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id        string `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	MaxSize   uint32 `protobuf:"varint,2,opt,name=max_size,json=maxSize,proto3" json:"max_size,omitempty"`
	Version   string `protobuf:"bytes,3,opt,name=version,proto3" json:"version,omitempty"`
	BuildInfo string `protobuf:"bytes,4,opt,name=build_info,json=buildInfo,proto3" json:"build_info,omitempty"`
}

func (x *InitResponse) Reset() {
	*x = InitResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_proto_instance_group_proto_msgTypes[4]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *InitResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*InitResponse) ProtoMessage() {}

func (x *InitResponse) ProtoReflect() protoreflect.Message {
	mi := &file_proto_instance_group_proto_msgTypes[4]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use InitResponse.ProtoReflect.Descriptor instead.
func (*InitResponse) Descriptor() ([]byte, []int) {
	return file_proto_instance_group_proto_rawDescGZIP(), []int{4}
}

func (x *InitResponse) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

func (x *InitResponse) GetMaxSize() uint32 {
	if x != nil {
		return x.MaxSize
	}
	return 0
}

func (x *InitResponse) GetVersion() string {
	if x != nil {
		return x.Version
	}
	return ""
}

func (x *InitResponse) GetBuildInfo() string {
	if x != nil {
		return x.BuildInfo
	}
	return ""
}

type UpdateResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Instance string               `protobuf:"bytes,1,opt,name=instance,proto3" json:"instance,omitempty"`
	State    UpdateResponse_State `protobuf:"varint,2,opt,name=state,proto3,enum=proto.UpdateResponse_State" json:"state,omitempty"`
}

func (x *UpdateResponse) Reset() {
	*x = UpdateResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_proto_instance_group_proto_msgTypes[5]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *UpdateResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*UpdateResponse) ProtoMessage() {}

func (x *UpdateResponse) ProtoReflect() protoreflect.Message {
	mi := &file_proto_instance_group_proto_msgTypes[5]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use UpdateResponse.ProtoReflect.Descriptor instead.
func (*UpdateResponse) Descriptor() ([]byte, []int) {
	return file_proto_instance_group_proto_rawDescGZIP(), []int{5}
}

func (x *UpdateResponse) GetInstance() string {
	if x != nil {
		return x.Instance
	}
	return ""
}

func (x *UpdateResponse) GetState() UpdateResponse_State {
	if x != nil {
		return x.State
	}
	return UpdateResponse_creating
}

type IncreaseRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	N uint32 `protobuf:"varint,1,opt,name=n,proto3" json:"n,omitempty"`
}

func (x *IncreaseRequest) Reset() {
	*x = IncreaseRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_proto_instance_group_proto_msgTypes[6]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *IncreaseRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*IncreaseRequest) ProtoMessage() {}

func (x *IncreaseRequest) ProtoReflect() protoreflect.Message {
	mi := &file_proto_instance_group_proto_msgTypes[6]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use IncreaseRequest.ProtoReflect.Descriptor instead.
func (*IncreaseRequest) Descriptor() ([]byte, []int) {
	return file_proto_instance_group_proto_rawDescGZIP(), []int{6}
}

func (x *IncreaseRequest) GetN() uint32 {
	if x != nil {
		return x.N
	}
	return 0
}

type IncreaseResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Succeeded uint32 `protobuf:"varint,1,opt,name=succeeded,proto3" json:"succeeded,omitempty"`
}

func (x *IncreaseResponse) Reset() {
	*x = IncreaseResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_proto_instance_group_proto_msgTypes[7]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *IncreaseResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*IncreaseResponse) ProtoMessage() {}

func (x *IncreaseResponse) ProtoReflect() protoreflect.Message {
	mi := &file_proto_instance_group_proto_msgTypes[7]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use IncreaseResponse.ProtoReflect.Descriptor instead.
func (*IncreaseResponse) Descriptor() ([]byte, []int) {
	return file_proto_instance_group_proto_rawDescGZIP(), []int{7}
}

func (x *IncreaseResponse) GetSucceeded() uint32 {
	if x != nil {
		return x.Succeeded
	}
	return 0
}

type DecreaseRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Instances []string `protobuf:"bytes,1,rep,name=instances,proto3" json:"instances,omitempty"`
}

func (x *DecreaseRequest) Reset() {
	*x = DecreaseRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_proto_instance_group_proto_msgTypes[8]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *DecreaseRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*DecreaseRequest) ProtoMessage() {}

func (x *DecreaseRequest) ProtoReflect() protoreflect.Message {
	mi := &file_proto_instance_group_proto_msgTypes[8]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use DecreaseRequest.ProtoReflect.Descriptor instead.
func (*DecreaseRequest) Descriptor() ([]byte, []int) {
	return file_proto_instance_group_proto_rawDescGZIP(), []int{8}
}

func (x *DecreaseRequest) GetInstances() []string {
	if x != nil {
		return x.Instances
	}
	return nil
}

type DecreaseResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Succeeded []string `protobuf:"bytes,1,rep,name=succeeded,proto3" json:"succeeded,omitempty"`
}

func (x *DecreaseResponse) Reset() {
	*x = DecreaseResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_proto_instance_group_proto_msgTypes[9]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *DecreaseResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*DecreaseResponse) ProtoMessage() {}

func (x *DecreaseResponse) ProtoReflect() protoreflect.Message {
	mi := &file_proto_instance_group_proto_msgTypes[9]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use DecreaseResponse.ProtoReflect.Descriptor instead.
func (*DecreaseResponse) Descriptor() ([]byte, []int) {
	return file_proto_instance_group_proto_rawDescGZIP(), []int{9}
}

func (x *DecreaseResponse) GetSucceeded() []string {
	if x != nil {
		return x.Succeeded
	}
	return nil
}

type ConnectInfoRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Instance string `protobuf:"bytes,1,opt,name=instance,proto3" json:"instance,omitempty"`
}

func (x *ConnectInfoRequest) Reset() {
	*x = ConnectInfoRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_proto_instance_group_proto_msgTypes[10]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *ConnectInfoRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*ConnectInfoRequest) ProtoMessage() {}

func (x *ConnectInfoRequest) ProtoReflect() protoreflect.Message {
	mi := &file_proto_instance_group_proto_msgTypes[10]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use ConnectInfoRequest.ProtoReflect.Descriptor instead.
func (*ConnectInfoRequest) Descriptor() ([]byte, []int) {
	return file_proto_instance_group_proto_rawDescGZIP(), []int{10}
}

func (x *ConnectInfoRequest) GetInstance() string {
	if x != nil {
		return x.Instance
	}
	return ""
}

type ConnectInfoResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Connector    *ConnectorConfig `protobuf:"bytes,1,opt,name=connector,proto3" json:"connector,omitempty"`
	ExternalAddr string           `protobuf:"bytes,2,opt,name=ExternalAddr,proto3" json:"ExternalAddr,omitempty"`
	InternalAddr string           `protobuf:"bytes,3,opt,name=InternalAddr,proto3" json:"InternalAddr,omitempty"`
	Expires      int64            `protobuf:"varint,4,opt,name=expires,proto3" json:"expires,omitempty"`
}

func (x *ConnectInfoResponse) Reset() {
	*x = ConnectInfoResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_proto_instance_group_proto_msgTypes[11]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *ConnectInfoResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*ConnectInfoResponse) ProtoMessage() {}

func (x *ConnectInfoResponse) ProtoReflect() protoreflect.Message {
	mi := &file_proto_instance_group_proto_msgTypes[11]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use ConnectInfoResponse.ProtoReflect.Descriptor instead.
func (*ConnectInfoResponse) Descriptor() ([]byte, []int) {
	return file_proto_instance_group_proto_rawDescGZIP(), []int{11}
}

func (x *ConnectInfoResponse) GetConnector() *ConnectorConfig {
	if x != nil {
		return x.Connector
	}
	return nil
}

func (x *ConnectInfoResponse) GetExternalAddr() string {
	if x != nil {
		return x.ExternalAddr
	}
	return ""
}

func (x *ConnectInfoResponse) GetInternalAddr() string {
	if x != nil {
		return x.InternalAddr
	}
	return ""
}

func (x *ConnectInfoResponse) GetExpires() int64 {
	if x != nil {
		return x.Expires
	}
	return 0
}

var File_proto_instance_group_proto protoreflect.FileDescriptor

var file_proto_instance_group_proto_rawDesc = []byte{
	0x0a, 0x1a, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2f, 0x69, 0x6e, 0x73, 0x74, 0x61, 0x6e, 0x63, 0x65,
	0x5f, 0x67, 0x72, 0x6f, 0x75, 0x70, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x05, 0x70, 0x72,
	0x6f, 0x74, 0x6f, 0x22, 0x07, 0x0a, 0x05, 0x45, 0x6d, 0x70, 0x74, 0x79, 0x22, 0x4d, 0x0a, 0x08,
	0x53, 0x65, 0x74, 0x74, 0x69, 0x6e, 0x67, 0x73, 0x12, 0x41, 0x0a, 0x10, 0x63, 0x6f, 0x6e, 0x6e,
	0x65, 0x63, 0x74, 0x6f, 0x72, 0x5f, 0x63, 0x6f, 0x6e, 0x66, 0x69, 0x67, 0x18, 0x01, 0x20, 0x01,
	0x28, 0x0b, 0x32, 0x16, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2e, 0x43, 0x6f, 0x6e, 0x6e, 0x65,
	0x63, 0x74, 0x6f, 0x72, 0x43, 0x6f, 0x6e, 0x66, 0x69, 0x67, 0x52, 0x0f, 0x63, 0x6f, 0x6e, 0x6e,
	0x65, 0x63, 0x74, 0x6f, 0x72, 0x43, 0x6f, 0x6e, 0x66, 0x69, 0x67, 0x22, 0x87, 0x02, 0x0a, 0x0f,
	0x43, 0x6f, 0x6e, 0x6e, 0x65, 0x63, 0x74, 0x6f, 0x72, 0x43, 0x6f, 0x6e, 0x66, 0x69, 0x67, 0x12,
	0x0e, 0x0a, 0x02, 0x6f, 0x73, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x02, 0x6f, 0x73, 0x12,
	0x12, 0x0a, 0x04, 0x61, 0x72, 0x63, 0x68, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x04, 0x61,
	0x72, 0x63, 0x68, 0x12, 0x1a, 0x0a, 0x08, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x63, 0x6f, 0x6c, 0x18,
	0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x08, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x63, 0x6f, 0x6c, 0x12,
	0x1a, 0x0a, 0x08, 0x75, 0x73, 0x65, 0x72, 0x6e, 0x61, 0x6d, 0x65, 0x18, 0x04, 0x20, 0x01, 0x28,
	0x09, 0x52, 0x08, 0x75, 0x73, 0x65, 0x72, 0x6e, 0x61, 0x6d, 0x65, 0x12, 0x1a, 0x0a, 0x08, 0x70,
	0x61, 0x73, 0x73, 0x77, 0x6f, 0x72, 0x64, 0x18, 0x05, 0x20, 0x01, 0x28, 0x09, 0x52, 0x08, 0x70,
	0x61, 0x73, 0x73, 0x77, 0x6f, 0x72, 0x64, 0x12, 0x10, 0x0a, 0x03, 0x6b, 0x65, 0x79, 0x18, 0x06,
	0x20, 0x01, 0x28, 0x0c, 0x52, 0x03, 0x6b, 0x65, 0x79, 0x12, 0x32, 0x0a, 0x14, 0x75, 0x73, 0x65,
	0x53, 0x74, 0x61, 0x74, 0x69, 0x63, 0x43, 0x72, 0x65, 0x64, 0x65, 0x6e, 0x74, 0x69, 0x61, 0x6c,
	0x73, 0x18, 0x07, 0x20, 0x01, 0x28, 0x08, 0x52, 0x14, 0x75, 0x73, 0x65, 0x53, 0x74, 0x61, 0x74,
	0x69, 0x63, 0x43, 0x72, 0x65, 0x64, 0x65, 0x6e, 0x74, 0x69, 0x61, 0x6c, 0x73, 0x12, 0x1c, 0x0a,
	0x09, 0x6b, 0x65, 0x65, 0x70, 0x61, 0x6c, 0x69, 0x76, 0x65, 0x18, 0x08, 0x20, 0x01, 0x28, 0x03,
	0x52, 0x09, 0x6b, 0x65, 0x65, 0x70, 0x61, 0x6c, 0x69, 0x76, 0x65, 0x12, 0x18, 0x0a, 0x07, 0x74,
	0x69, 0x6d, 0x65, 0x6f, 0x75, 0x74, 0x18, 0x09, 0x20, 0x01, 0x28, 0x03, 0x52, 0x07, 0x74, 0x69,
	0x6d, 0x65, 0x6f, 0x75, 0x74, 0x22, 0x52, 0x0a, 0x0b, 0x49, 0x6e, 0x69, 0x74, 0x52, 0x65, 0x71,
	0x75, 0x65, 0x73, 0x74, 0x12, 0x16, 0x0a, 0x06, 0x63, 0x6f, 0x6e, 0x66, 0x69, 0x67, 0x18, 0x01,
	0x20, 0x01, 0x28, 0x0c, 0x52, 0x06, 0x63, 0x6f, 0x6e, 0x66, 0x69, 0x67, 0x12, 0x2b, 0x0a, 0x08,
	0x73, 0x65, 0x74, 0x74, 0x69, 0x6e, 0x67, 0x73, 0x18, 0x02, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x0f,
	0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2e, 0x53, 0x65, 0x74, 0x74, 0x69, 0x6e, 0x67, 0x73, 0x52,
	0x08, 0x73, 0x65, 0x74, 0x74, 0x69, 0x6e, 0x67, 0x73, 0x22, 0x72, 0x0a, 0x0c, 0x49, 0x6e, 0x69,
	0x74, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18,
	0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x02, 0x69, 0x64, 0x12, 0x19, 0x0a, 0x08, 0x6d, 0x61, 0x78,
	0x5f, 0x73, 0x69, 0x7a, 0x65, 0x18, 0x02, 0x20, 0x01, 0x28, 0x0d, 0x52, 0x07, 0x6d, 0x61, 0x78,
	0x53, 0x69, 0x7a, 0x65, 0x12, 0x18, 0x0a, 0x07, 0x76, 0x65, 0x72, 0x73, 0x69, 0x6f, 0x6e, 0x18,
	0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x07, 0x76, 0x65, 0x72, 0x73, 0x69, 0x6f, 0x6e, 0x12, 0x1d,
	0x0a, 0x0a, 0x62, 0x75, 0x69, 0x6c, 0x64, 0x5f, 0x69, 0x6e, 0x66, 0x6f, 0x18, 0x04, 0x20, 0x01,
	0x28, 0x09, 0x52, 0x09, 0x62, 0x75, 0x69, 0x6c, 0x64, 0x49, 0x6e, 0x66, 0x6f, 0x22, 0xab, 0x01,
	0x0a, 0x0e, 0x55, 0x70, 0x64, 0x61, 0x74, 0x65, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65,
	0x12, 0x1a, 0x0a, 0x08, 0x69, 0x6e, 0x73, 0x74, 0x61, 0x6e, 0x63, 0x65, 0x18, 0x01, 0x20, 0x01,
	0x28, 0x09, 0x52, 0x08, 0x69, 0x6e, 0x73, 0x74, 0x61, 0x6e, 0x63, 0x65, 0x12, 0x31, 0x0a, 0x05,
	0x73, 0x74, 0x61, 0x74, 0x65, 0x18, 0x02, 0x20, 0x01, 0x28, 0x0e, 0x32, 0x1b, 0x2e, 0x70, 0x72,
	0x6f, 0x74, 0x6f, 0x2e, 0x55, 0x70, 0x64, 0x61, 0x74, 0x65, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e,
	0x73, 0x65, 0x2e, 0x53, 0x74, 0x61, 0x74, 0x65, 0x52, 0x05, 0x73, 0x74, 0x61, 0x74, 0x65, 0x22,
	0x4a, 0x0a, 0x05, 0x53, 0x74, 0x61, 0x74, 0x65, 0x12, 0x0c, 0x0a, 0x08, 0x63, 0x72, 0x65, 0x61,
	0x74, 0x69, 0x6e, 0x67, 0x10, 0x00, 0x12, 0x0b, 0x0a, 0x07, 0x72, 0x75, 0x6e, 0x6e, 0x69, 0x6e,
	0x67, 0x10, 0x01, 0x12, 0x0c, 0x0a, 0x08, 0x64, 0x65, 0x6c, 0x65, 0x74, 0x69, 0x6e, 0x67, 0x10,
	0x02, 0x12, 0x0b, 0x0a, 0x07, 0x64, 0x65, 0x6c, 0x65, 0x74, 0x65, 0x64, 0x10, 0x03, 0x12, 0x0b,
	0x0a, 0x07, 0x74, 0x69, 0x6d, 0x65, 0x6f, 0x75, 0x74, 0x10, 0x04, 0x22, 0x1f, 0x0a, 0x0f, 0x49,
	0x6e, 0x63, 0x72, 0x65, 0x61, 0x73, 0x65, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x0c,
	0x0a, 0x01, 0x6e, 0x18, 0x01, 0x20, 0x01, 0x28, 0x0d, 0x52, 0x01, 0x6e, 0x22, 0x30, 0x0a, 0x10,
	0x49, 0x6e, 0x63, 0x72, 0x65, 0x61, 0x73, 0x65, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65,
	0x12, 0x1c, 0x0a, 0x09, 0x73, 0x75, 0x63, 0x63, 0x65, 0x65, 0x64, 0x65, 0x64, 0x18, 0x01, 0x20,
	0x01, 0x28, 0x0d, 0x52, 0x09, 0x73, 0x75, 0x63, 0x63, 0x65, 0x65, 0x64, 0x65, 0x64, 0x22, 0x2f,
	0x0a, 0x0f, 0x44, 0x65, 0x63, 0x72, 0x65, 0x61, 0x73, 0x65, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73,
	0x74, 0x12, 0x1c, 0x0a, 0x09, 0x69, 0x6e, 0x73, 0x74, 0x61, 0x6e, 0x63, 0x65, 0x73, 0x18, 0x01,
	0x20, 0x03, 0x28, 0x09, 0x52, 0x09, 0x69, 0x6e, 0x73, 0x74, 0x61, 0x6e, 0x63, 0x65, 0x73, 0x22,
	0x30, 0x0a, 0x10, 0x44, 0x65, 0x63, 0x72, 0x65, 0x61, 0x73, 0x65, 0x52, 0x65, 0x73, 0x70, 0x6f,
	0x6e, 0x73, 0x65, 0x12, 0x1c, 0x0a, 0x09, 0x73, 0x75, 0x63, 0x63, 0x65, 0x65, 0x64, 0x65, 0x64,
	0x18, 0x01, 0x20, 0x03, 0x28, 0x09, 0x52, 0x09, 0x73, 0x75, 0x63, 0x63, 0x65, 0x65, 0x64, 0x65,
	0x64, 0x22, 0x30, 0x0a, 0x12, 0x43, 0x6f, 0x6e, 0x6e, 0x65, 0x63, 0x74, 0x49, 0x6e, 0x66, 0x6f,
	0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x1a, 0x0a, 0x08, 0x69, 0x6e, 0x73, 0x74, 0x61,
	0x6e, 0x63, 0x65, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x08, 0x69, 0x6e, 0x73, 0x74, 0x61,
	0x6e, 0x63, 0x65, 0x22, 0xad, 0x01, 0x0a, 0x13, 0x43, 0x6f, 0x6e, 0x6e, 0x65, 0x63, 0x74, 0x49,
	0x6e, 0x66, 0x6f, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x34, 0x0a, 0x09, 0x63,
	0x6f, 0x6e, 0x6e, 0x65, 0x63, 0x74, 0x6f, 0x72, 0x18, 0x01, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x16,
	0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2e, 0x43, 0x6f, 0x6e, 0x6e, 0x65, 0x63, 0x74, 0x6f, 0x72,
	0x43, 0x6f, 0x6e, 0x66, 0x69, 0x67, 0x52, 0x09, 0x63, 0x6f, 0x6e, 0x6e, 0x65, 0x63, 0x74, 0x6f,
	0x72, 0x12, 0x22, 0x0a, 0x0c, 0x45, 0x78, 0x74, 0x65, 0x72, 0x6e, 0x61, 0x6c, 0x41, 0x64, 0x64,
	0x72, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0c, 0x45, 0x78, 0x74, 0x65, 0x72, 0x6e, 0x61,
	0x6c, 0x41, 0x64, 0x64, 0x72, 0x12, 0x22, 0x0a, 0x0c, 0x49, 0x6e, 0x74, 0x65, 0x72, 0x6e, 0x61,
	0x6c, 0x41, 0x64, 0x64, 0x72, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0c, 0x49, 0x6e, 0x74,
	0x65, 0x72, 0x6e, 0x61, 0x6c, 0x41, 0x64, 0x64, 0x72, 0x12, 0x18, 0x0a, 0x07, 0x65, 0x78, 0x70,
	0x69, 0x72, 0x65, 0x73, 0x18, 0x04, 0x20, 0x01, 0x28, 0x03, 0x52, 0x07, 0x65, 0x78, 0x70, 0x69,
	0x72, 0x65, 0x73, 0x32, 0xd9, 0x02, 0x0a, 0x0d, 0x49, 0x6e, 0x73, 0x74, 0x61, 0x6e, 0x63, 0x65,
	0x47, 0x72, 0x6f, 0x75, 0x70, 0x12, 0x2f, 0x0a, 0x04, 0x49, 0x6e, 0x69, 0x74, 0x12, 0x12, 0x2e,
	0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2e, 0x49, 0x6e, 0x69, 0x74, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73,
	0x74, 0x1a, 0x13, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2e, 0x49, 0x6e, 0x69, 0x74, 0x52, 0x65,
	0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x2f, 0x0a, 0x06, 0x55, 0x70, 0x64, 0x61, 0x74, 0x65,
	0x12, 0x0c, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2e, 0x45, 0x6d, 0x70, 0x74, 0x79, 0x1a, 0x15,
	0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2e, 0x55, 0x70, 0x64, 0x61, 0x74, 0x65, 0x52, 0x65, 0x73,
	0x70, 0x6f, 0x6e, 0x73, 0x65, 0x30, 0x01, 0x12, 0x3b, 0x0a, 0x08, 0x49, 0x6e, 0x63, 0x72, 0x65,
	0x61, 0x73, 0x65, 0x12, 0x16, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2e, 0x49, 0x6e, 0x63, 0x72,
	0x65, 0x61, 0x73, 0x65, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x17, 0x2e, 0x70, 0x72,
	0x6f, 0x74, 0x6f, 0x2e, 0x49, 0x6e, 0x63, 0x72, 0x65, 0x61, 0x73, 0x65, 0x52, 0x65, 0x73, 0x70,
	0x6f, 0x6e, 0x73, 0x65, 0x12, 0x3b, 0x0a, 0x08, 0x44, 0x65, 0x63, 0x72, 0x65, 0x61, 0x73, 0x65,
	0x12, 0x16, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2e, 0x44, 0x65, 0x63, 0x72, 0x65, 0x61, 0x73,
	0x65, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x17, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f,
	0x2e, 0x44, 0x65, 0x63, 0x72, 0x65, 0x61, 0x73, 0x65, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73,
	0x65, 0x12, 0x44, 0x0a, 0x0b, 0x43, 0x6f, 0x6e, 0x6e, 0x65, 0x63, 0x74, 0x49, 0x6e, 0x66, 0x6f,
	0x12, 0x19, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2e, 0x43, 0x6f, 0x6e, 0x6e, 0x65, 0x63, 0x74,
	0x49, 0x6e, 0x66, 0x6f, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x1a, 0x2e, 0x70, 0x72,
	0x6f, 0x74, 0x6f, 0x2e, 0x43, 0x6f, 0x6e, 0x6e, 0x65, 0x63, 0x74, 0x49, 0x6e, 0x66, 0x6f, 0x52,
	0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x26, 0x0a, 0x08, 0x53, 0x68, 0x75, 0x74, 0x64,
	0x6f, 0x77, 0x6e, 0x12, 0x0c, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2e, 0x45, 0x6d, 0x70, 0x74,
	0x79, 0x1a, 0x0c, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2e, 0x45, 0x6d, 0x70, 0x74, 0x79, 0x42,
	0x09, 0x5a, 0x07, 0x2e, 0x2f, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74,
	0x6f, 0x33,
}

var (
	file_proto_instance_group_proto_rawDescOnce sync.Once
	file_proto_instance_group_proto_rawDescData = file_proto_instance_group_proto_rawDesc
)

func file_proto_instance_group_proto_rawDescGZIP() []byte {
	file_proto_instance_group_proto_rawDescOnce.Do(func() {
		file_proto_instance_group_proto_rawDescData = protoimpl.X.CompressGZIP(file_proto_instance_group_proto_rawDescData)
	})
	return file_proto_instance_group_proto_rawDescData
}

var file_proto_instance_group_proto_enumTypes = make([]protoimpl.EnumInfo, 1)
var file_proto_instance_group_proto_msgTypes = make([]protoimpl.MessageInfo, 12)
var file_proto_instance_group_proto_goTypes = []interface{}{
	(UpdateResponse_State)(0),   // 0: proto.UpdateResponse.State
	(*Empty)(nil),               // 1: proto.Empty
	(*Settings)(nil),            // 2: proto.Settings
	(*ConnectorConfig)(nil),     // 3: proto.ConnectorConfig
	(*InitRequest)(nil),         // 4: proto.InitRequest
	(*InitResponse)(nil),        // 5: proto.InitResponse
	(*UpdateResponse)(nil),      // 6: proto.UpdateResponse
	(*IncreaseRequest)(nil),     // 7: proto.IncreaseRequest
	(*IncreaseResponse)(nil),    // 8: proto.IncreaseResponse
	(*DecreaseRequest)(nil),     // 9: proto.DecreaseRequest
	(*DecreaseResponse)(nil),    // 10: proto.DecreaseResponse
	(*ConnectInfoRequest)(nil),  // 11: proto.ConnectInfoRequest
	(*ConnectInfoResponse)(nil), // 12: proto.ConnectInfoResponse
}
var file_proto_instance_group_proto_depIdxs = []int32{
	3,  // 0: proto.Settings.connector_config:type_name -> proto.ConnectorConfig
	2,  // 1: proto.InitRequest.settings:type_name -> proto.Settings
	0,  // 2: proto.UpdateResponse.state:type_name -> proto.UpdateResponse.State
	3,  // 3: proto.ConnectInfoResponse.connector:type_name -> proto.ConnectorConfig
	4,  // 4: proto.InstanceGroup.Init:input_type -> proto.InitRequest
	1,  // 5: proto.InstanceGroup.Update:input_type -> proto.Empty
	7,  // 6: proto.InstanceGroup.Increase:input_type -> proto.IncreaseRequest
	9,  // 7: proto.InstanceGroup.Decrease:input_type -> proto.DecreaseRequest
	11, // 8: proto.InstanceGroup.ConnectInfo:input_type -> proto.ConnectInfoRequest
	1,  // 9: proto.InstanceGroup.Shutdown:input_type -> proto.Empty
	5,  // 10: proto.InstanceGroup.Init:output_type -> proto.InitResponse
	6,  // 11: proto.InstanceGroup.Update:output_type -> proto.UpdateResponse
	8,  // 12: proto.InstanceGroup.Increase:output_type -> proto.IncreaseResponse
	10, // 13: proto.InstanceGroup.Decrease:output_type -> proto.DecreaseResponse
	12, // 14: proto.InstanceGroup.ConnectInfo:output_type -> proto.ConnectInfoResponse
	1,  // 15: proto.InstanceGroup.Shutdown:output_type -> proto.Empty
	10, // [10:16] is the sub-list for method output_type
	4,  // [4:10] is the sub-list for method input_type
	4,  // [4:4] is the sub-list for extension type_name
	4,  // [4:4] is the sub-list for extension extendee
	0,  // [0:4] is the sub-list for field type_name
}

func init() { file_proto_instance_group_proto_init() }
func file_proto_instance_group_proto_init() {
	if File_proto_instance_group_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_proto_instance_group_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*Empty); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_proto_instance_group_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*Settings); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_proto_instance_group_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*ConnectorConfig); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_proto_instance_group_proto_msgTypes[3].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*InitRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_proto_instance_group_proto_msgTypes[4].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*InitResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_proto_instance_group_proto_msgTypes[5].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*UpdateResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_proto_instance_group_proto_msgTypes[6].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*IncreaseRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_proto_instance_group_proto_msgTypes[7].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*IncreaseResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_proto_instance_group_proto_msgTypes[8].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*DecreaseRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_proto_instance_group_proto_msgTypes[9].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*DecreaseResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_proto_instance_group_proto_msgTypes[10].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*ConnectInfoRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_proto_instance_group_proto_msgTypes[11].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*ConnectInfoResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_proto_instance_group_proto_rawDesc,
			NumEnums:      1,
			NumMessages:   12,
			NumExtensions: 0,
			NumServices:   1,
		},
		GoTypes:           file_proto_instance_group_proto_goTypes,
		DependencyIndexes: file_proto_instance_group_proto_depIdxs,
		EnumInfos:         file_proto_instance_group_proto_enumTypes,
		MessageInfos:      file_proto_instance_group_proto_msgTypes,
	}.Build()
	File_proto_instance_group_proto = out.File
	file_proto_instance_group_proto_rawDesc = nil
	file_proto_instance_group_proto_goTypes = nil
	file_proto_instance_group_proto_depIdxs = nil
}
