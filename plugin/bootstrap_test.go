package plugin

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNormalizeRepo(t *testing.T) {
	tests := map[string]string{
		"index.docker.io/namespace/project":                   "index.docker.io/namespace/project",
		"registry.gitlab.com/namespace/project":               "registry.gitlab.com/namespace/project",
		"namespace/no-registry":                               "registry.gitlab.com/namespace/no-registry",
		"long/namespace/no-registry":                          "registry.gitlab.com/long/namespace/no-registry",
		"no-registry-or-namespace":                            "registry.gitlab.com/gitlab-org/fleeting/plugins/no-registry-or-namespace",
		"drop-version:1.0.0":                                  "registry.gitlab.com/gitlab-org/fleeting/plugins/drop-version",
		"registry.gitlab.com/drop-version/project:1.2.3":      "registry.gitlab.com/drop-version/project",
		"registry.gitlab.com:8080/drop-version/project:1.2.3": "registry.gitlab.com:8080/drop-version/project",
	}

	for input, expected := range tests {
		t.Run(input, func(t *testing.T) {
			assert.Equal(t, expected, normalizeRepo(input))
		})
	}
}
