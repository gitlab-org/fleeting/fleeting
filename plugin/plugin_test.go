package plugin

import (
	"os/exec"
	"path/filepath"
	"strings"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-org/fleeting/fleeting/integration"
)

func TestPluginMain(t *testing.T) {
	binaryName := integration.BuildPluginBinary(t, "fleeting-plugin-dummy/cmd/fleeting-plugin-dummy", "fleeting-plugin-dummy")

	tests := []struct {
		args   []string
		assert func(t *testing.T, args []string)
	}{
		{
			args: []string{"-version", "version"},
			assert: func(t *testing.T, args []string) {
				out, err := exec.Command(binaryName, args...).CombinedOutput()
				require.NoError(t, err, string(out))
				require.Contains(t, string(out), "Git revision")
				require.Contains(t, string(out), "dev")
			},
		},
		{
			args: []string{"bootstrap foo.bar/foo"},
			assert: func(t *testing.T, args []string) {
				pluginDir := t.TempDir()

				cmd := exec.Command(binaryName, args...)
				cmd.Env = append(cmd.Env, "FLEETING_PLUGIN_PATH="+pluginDir)
				out, err := cmd.CombinedOutput()

				require.NoError(t, err, string(out))
				require.Contains(t, string(out), filepath.Join(pluginDir, "foo.bar", "foo", "0.0.0-"))
			},
		},
		{
			args: []string{"bootstrap repository/something"},
			assert: func(t *testing.T, args []string) {
				pluginDir := t.TempDir()

				cmd := exec.Command(binaryName, args...)
				cmd.Env = append(cmd.Env, "FLEETING_PLUGIN_PATH="+pluginDir)
				out, err := cmd.CombinedOutput()

				require.NoError(t, err, string(out))
				require.Contains(t, string(out), filepath.Join(pluginDir, "registry.gitlab.com", "repository", "something", "0.0.0-"))
			},
		},
		{
			args: []string{"bootstrap official"},
			assert: func(t *testing.T, args []string) {
				pluginDir := t.TempDir()

				cmd := exec.Command(binaryName, args...)
				cmd.Env = append(cmd.Env, "FLEETING_PLUGIN_PATH="+pluginDir)
				out, err := cmd.CombinedOutput()

				require.NoError(t, err, string(out))
				require.Contains(t, string(out), filepath.Join(pluginDir, "registry.gitlab.com", "gitlab-org", "fleeting", "plugins", "official", "0.0.0-"))
			},
		},
		{
			args: []string{"bootstrap dev"},
			assert: func(t *testing.T, args []string) {
				pluginDir := t.TempDir()

				cmd := exec.Command(binaryName, args...)
				cmd.Env = append(cmd.Env, "FLEETING_PLUGIN_PATH="+pluginDir, "FLEETING_PLUGIN_PKG_VERSION=dev")
				out, err := cmd.CombinedOutput()

				require.NoError(t, err, string(out))
				require.Contains(t, string(out), filepath.Join(pluginDir, "registry.gitlab.com", "gitlab-org", "fleeting", "plugins", "dev", "0.0.0-"))
				require.NotContains(t, string(out), "fleeting plugin package version and binary version differ")
			},
		},
		{
			args: []string{"bootstrap diff-version"},
			assert: func(t *testing.T, args []string) {
				pluginDir := t.TempDir()

				cmd := exec.Command(binaryName, args...)
				cmd.Env = append(cmd.Env, "FLEETING_PLUGIN_PATH="+pluginDir, "FLEETING_PLUGIN_PKG_VERSION=0.0.0")
				out, err := cmd.CombinedOutput()

				require.NoError(t, err, string(out))
				require.Contains(t, string(out), filepath.Join(pluginDir, "registry.gitlab.com", "gitlab-org", "fleeting", "plugins", "diff-version", "0.0.0"))
				require.Contains(t, string(out), "fleeting plugin package version and binary version differ")
			},
		},
		{
			args: []string{"serve", ""},
			assert: func(t *testing.T, args []string) {
				out, err := exec.Command(binaryName, args...).CombinedOutput()
				require.Error(t, err)
				require.Contains(t, string(out), "This binary is a plugin.")
			},
		},
	}

	for _, tc := range tests {
		for _, arg := range tc.args {
			t.Run(arg, func(t *testing.T) {
				tc.assert(t, strings.Split(arg, " "))
			})
		}
	}
}
